import os
import glob
from openpyxl import load_workbook
from openpyxl.styles import Font
from openpyxl.styles import Alignment
from openpyxl.workbook import Workbook
import sys
import time

def getXLfiles():
    print("Files Found:")
    files = glob.glob("*.xlsx")
    for i in range(0, len(files)):
        print("[%d] %s" % (i, files[i]))

    UserInputValid = False

    while(not UserInputValid):
        userinput = input("Please Select Old Excel File (Enter Assigned Number):")
        try:
            temp = int(userinput)
            if temp in range(0, len(files)):
                print(files[temp], "was selected\n")
                UserInputValid = True
                oldfile = files[temp]
            else:
                print("Please Enter Number within Range of Files\n")
        except ValueError:
            print("Please Enter a Number\n")

    UserInputValid = False

    while(not UserInputValid):
        userinput = input("Now Select New Excel File You Want to Compare to Old File (Enter Assigned Number):")
        try:
            temp = int(userinput)
            if temp in range(0, len(files)):
                print(files[temp], "was selected\n")
                UserInputValid = True
                newfile = files[temp]
                time.sleep(1)
            else:
                print("Please Enter Number within Range of Files\n")
        except ValueError:
            print("Please Enter a Number\n")

    if newfile == oldfile:
        print("You Must Select Differnt Files!")
        print("Program Will Close In 3 Seconds")
        time.sleep(3)
        sys.exit()
    os.system('cls')
    print("Old File: ", oldfile)
    print("New File: ", newfile)
    return oldfile, newfile


def writeExcel(outQ, wb, sheet, filename):
    cell_title = sheet['O1']
    cell_title.value = "Room"
    cell_title.font = Font(bold=True)
    cell_title.alignment = Alignment(horizontal="center", vertical="center")
    cell_title = sheet['P1']
    cell_title.value = "Instructor"
    cell_title.font = Font(bold=True)
    cell_title.alignment = Alignment(horizontal="center", vertical="center")

    while not outQ.empty():
        i = outQ.get()
        sheet.cell(row=i[0], column=15).value = i[1]
        sheet.cell(row=i[0], column=16).value = i[2]

    wb.save(filename + ".xlsx")

def loadexcel(name):
    data = []
    try:
        wb = load_workbook(name)
    except:
        print("Please rename excel document to data.xlsx")
        time.sleep(10)
        sys.exit()

    first_sheet = wb.get_sheet_names()[0]
    worksheet = wb.get_sheet_by_name(first_sheet)
    cell_title = worksheet['O1']
    cell_title.value = "Room"
    cell_title.font = Font(bold=True)
    cell_title.alignment = Alignment(horizontal="center", vertical="center")
    for row in range(2, worksheet.max_row + 1):
        id = (worksheet.cell(row=row, column=1).value)
        data.append([row, id])
    return data, wb, worksheet

class Excel:
    def __init__(self, fileName):
        self.filename = fileName
        self.wb = None
        self.ws = None
        self.ids = {}
        self.updated = None
        self.removed = None
        self.new = None

    def setupfile(self):
        self.wb = Workbook(write_only=True)
        self.updated = self.wb.create_sheet("Updated")
        self.removed = self.wb.create_sheet("Removed")
        self.new = self.wb.create_sheet("New")

    def add_new(self, data):
        self.new.append(data)

    def add_removed(self, data):
        self.removed.append(data)

    def add_updated(self, dataold, datanew):
        self.updated.append(dataold)
        self.updated.append(datanew)
        self.updated.append([""])

    def loadfile(self):
        self.wb = load_workbook(self.filename)
        first_sheet = self.wb.get_sheet_names()[0]
        self.ws = self.wb.get_sheet_by_name(first_sheet)

    def getRow(self, rownumber):
        data = []
        for column in range(1, self.ws.max_column + 1):
            temp = self.ws.cell(row=rownumber, column=column).value
            data.append(temp)
        return data

    def getIDs(self):
        for row in range(2, self.ws.max_row + 1):
            id = (self.ws.cell(row=row, column=1).value)
            self.ids[id] = row

    def savefile(self):
        self.wb.save(self.filename + ".xlsx")