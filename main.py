import queue


from Excel import *
from Menu import *
import datetime
from multiprocessing import Queue

Menu_Instructions()

# Loading Excel Files
oldfile, newfile = getXLfiles()

OldExcel = Excel(oldfile)
NewExcel = Excel(newfile)

OldExcel.loadfile()
OldExcel.getIDs()

NewExcel.loadfile()
NewExcel.getIDs()

# Set Up File
ChangeExcel = Excel("Changes Tracked")
ChangeExcel.setupfile()


#Get keys from New Excel (Employee ID)
for key in NewExcel.ids.keys():

    newrownumber = NewExcel.ids[key]
    newrow = NewExcel.getRow(newrownumber)

    try:
        oldrownumber = OldExcel.ids[key]
        oldrow = OldExcel.getRow(oldrownumber)
    except KeyError:
        # Key not not present in old excel, meaning that its a new entry
        ChangeExcel.add_new(newrow)
        continue

    if oldrow != newrow:
        print("mismatch")
        print(oldrow)
        print(newrow)
        ChangeExcel.add_updated(oldrow, newrow)


# Checking for rows that are present in old, but not in new
for key in OldExcel.ids.keys():
    oldrownumber = OldExcel.ids[key]
    oldrow = OldExcel.getRow(oldrownumber)
    try:
        newrownumber = NewExcel.ids[key]
    except KeyError:
        ChangeExcel.add_removed(oldrow)
        continue

ChangeExcel.savefile()